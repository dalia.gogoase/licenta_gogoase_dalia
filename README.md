# Licența_Gogoase_Dalia
## Sistem Digital pentru Siguranță și Confort Casnic

### Descriere
Sistemul Digital pentru Siguranță și Confort Casnic este o aplicație Android dezvoltată în Android Studio, care îmbunătățește securitatea și confortul locuinței prin utilizarea unui sistem de blocare inteligentă bazat pe Arduino și controlat prin amprentă și aplicație mobilă. Utilizatorii pot adăuga, gestiona și utiliza amprente pentru a debloca ușa, primind notificări în timp real despre starea ușii.

### Funcționalități
- **Deschide ușa pe bază de amprentă**: Recunoaștere biometrică de înaltă rezoluție a amprentei pentru acces securizat.
- **Deschiderea ușii pe bază de buton**: Această funcționalitate a fost adăugată pentru deschiderea ușii din interiorul casei.
- **Conectarea prin bluetooth la circuit**: Conectarea prin bluetooth la circuit din intermediul aplicației prin apăsarea unui buton.
- **Deschiderea ușii din aplicație**: Presupune apăsarea unui buton din aplicație care trimite un mesaj la circuit.
- **Vizualizarea istoricului deschiderilor ușii**: Este folosită o bază de date unde se înregistrează câte o instanță de fiecare dată când se deschide ușa conținând informații legate de momentul deschiderii, utilizatorul și ușa care a fost deschisă.

### Instalare
Pentru a instala și rula aplicația, urmează acești pași:
**Instalează Android Studio IDE**

**Clonează repository-ul**:
Rulează această comandă folosind git Bash:
  git clone https://gitlab.upt.ro/dalia.gogoase/licenta_gogoase_dalia.git

**Deschide proiectul în Android Studio:**:
File > Open > Select the project directory.

**Construiește proiectul**:
Folosește butonul de build în Android Studio sau rulează comanda ./gradlew build din terminal.

**Rulează aplicația pe un emulator sau dispozitiv fizic:**:
Conectează un dispozitiv prin USB sau pornește un emulator.
Apasă butonul de run în Android Studio sau rulează comanda ./gradlew installDebug.

## Contribuții
Nu sunt deschisă la contribuții.

## Autori și mulțumiri
Gogoase Dalia.
